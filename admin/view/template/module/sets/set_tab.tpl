<div class="tab-pane" id="tab-set">

    <input type='hidden' name='product_id' value='<?php echo $product_id;?>'>
    <div class='save_result'></div>
    <button id='add_set' class='btn btn-complete'><?php echo $entry_add_set;?></button>
    <button id='save_sets' class='btn btn-success'><?php echo $entry_save_sets;?></button>


    <div class='clearfix'></div>
    <?php if(isset($sets)) { ?>
    <?php echo $sets;?>
    <script>
        $(document).ready(function () {
            $('.set').find('.search_product_name input[name="product_name"]').autocomplete({'source': autocomplete_source,'select': autocomplete_select});
        });
    </script>
    <?php } ?>

</div>
<script>
    var c;

    $("#tab-set").on('click', '#save_sets', function () {
        var data = $("#tab-set").find('select, textarea, input').serialize();
        $.ajax({
            url: 'index.php?route=module/sets/saveSets&token=<?php echo $token; ?>',
            method: 'POST',
            data: data,
            success: function (json) {

                if (json['error']) {
                    $("#tab-set .save_result").html('<div class=\'alert alert-danger\'>' + json['error'] + '</div>')
                }

                if (json['success']) {
                    $("#tab-set .save_result").html('<div class=\'alert alert-success\'>' + json['success'] + '</div>')
                }
            }
        });
        return false;
    });

    function getKey()
    {
        if ($('.set').length > 0)
        {
            maximum = 0;
            $('.set').each(function () {
                var value = parseFloat($(this).attr('key'));
                maximum = (value > maximum) ? value : maximum;
            });

            ++maximum;
            return maximum;
        } else
            return 1;
    }

    $("#tab-set").on('click', '#add_set', function () {
        var set_table = "<?php echo $set_clear_form;?>";
        var key = getKey();
        set_table = set_table.replace(/{si}/g, key);
        set_table = $(set_table).attr('key', key);

        $(set_table).find('.search_product_name input[name="product_name"]').autocomplete({'source': autocomplete_source,'select': autocomplete_select});
        $("#tab-set").append(set_table);
        return false;
    });

    $('#tab-set').on('click', '.set .del_product', function () {
        $(this).parents('tr').remove();
        update_total();
        return false;
    });

    $('#tab-set').on('click', '.set .del_set', function () {
        $(this).parents('.set').remove();
        return false;
    });


    $('#tab-set').on('change', '.set .new_price input', function () {
        c = $(this).parents('.set');
        update_new_summ();
    });

    $('#tab-set').on('change', '.set .quantity input', function () {
        c = $(this).parents('.set');

        update_old_summ();
        update_new_summ();
    });


    function update_old_summ()
    {
        var summ = 0;
        var prices = $(c).find('.old_price input');
        var qtys = $(c).find('.quantity input');

        for (var i = 0; i < qtys.length; i++)
            summ += parseFloat($(qtys).eq(i).val()) * parseFloat($(prices).eq(i).val());

        $(c).find(".old_summ input").val(summ);
    }

    function update_new_summ()
    {
        var summ = 0;
        var prices = $(c).find('.new_price input');
        var qtys = $(c).find('.quantity input');

        for (var i = 0; i < qtys.length; i++)
            summ += parseFloat($(qtys).eq(i).val()) * parseFloat($(prices).eq(i).val());
        $(c).find(".new_summ input").val(summ);
    }

    function update_total()
    {
        update_old_summ();
        update_new_summ();
    }
    function get_new_row(key, numrow)
    {
        var row = "<?php echo $set_row;?>";
        row = row.replace(/{si}/g, key);
        row = row.replace(/{pi}/g, numrow);
        return row;
    }

    function autocomplete_source(request, response) {
        $.ajax({
            url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
            dataType: 'json',
            success: function (json) {
                response($.map(json, function (item) {
                    return {
                        label: item['name'],
                        value: item['product_id'],
                        price: item['price'],
                        option: item['option']
                    }
                }));
            }
        });
    }

    function autocomplete_select(item) {

        c = $(this).parents('.set');
        numrow = $(c).find('.product_row').size() + 1;
        key = $(c).attr('key');
        var new_row = $(get_new_row(key, numrow));
        $(new_row).find('.product_name input[type="text"]').val(item['label']);
        $(new_row).find('.product_name input[type="hidden"]').val(item['value']);
        $(new_row).find('.old_price input').val(item['price']);
        $(new_row).find('.new_price input').val(item['price']);

        if (item['option'].length)
            $.ajax({
                method: 'POST',
                url: 'index.php?route=module/sets/optionsForms&token=<?php echo $token; ?>',
                data: {options: item['option']},
                success: function (data) {
                    data = data.replace(/{si}/g, key);
                    data = data.replace(/{pi}/g, numrow);
                    $(new_row).find('.options .modal .modal-body').html(data);
                }
            });

        else
        {
            $(new_row).find('.options button').addClass('disabled');
            $(new_row).find('.options button,.options input').prop('disabled', true);
            
        }

        $(c).find('.search_row').before(new_row);

        update_total();
    }
</script>
<style>
    .set
    {
        max-width: 700px;
        margin:10px auto;
    }
    .set .table
    {
        background-color: #F5F5F5;
    }
    .set .total input
    {
        font-size: 16px;
        font-weight: bold;
    }
    .set .total .old_summ input
    {
        color:red;
    }
    .set .total .new_summ input
    {
        color:green;
    }
    .set .product_name
    {
        font-weight: bold;
    }

</style>