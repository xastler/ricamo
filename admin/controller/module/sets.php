<?php

class ControllerModuleSets extends Controller
{

    private $error = array();

    public function install()
    {
        $this->db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD `sets` text NULL;");
    }

    public function getSetsForm()
    {
        $newline_symbols = array("\r", "\n");

        $this->load->language('module/sets');

        if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST'))
        {
            $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
        }

        if (isset($product_info))
        {
            $set_clear_from['product_name'] = $product_info['name'];
            $set_clear_from['product_price'] = $product_info['price'];
            $set_clear_from['product_id'] = $product_info['product_id'];
            $set_clear_from['options'] = $this->getOptProduct($product_info['product_id']);
            $set_tab['product_id'] = $product_info['product_id'];
        } else
        {
            $set_clear_from['product_name'] = '';
            $set_clear_from['product_price'] = 0;
            $set_clear_from['product_id'] = 0;
            $set_clear_from['options'] = array();
            $set_tab['product_id'] = 0;
        }

        $set_clear_from_text['entry_product_name'] = $this->language->get('entry_name');
        $set_clear_from_text['entry_product_pr'] = $this->language->get('entry_price');
        $set_clear_from_text['entry_product_quantity'] = $this->language->get('entry_quantity');
        $set_clear_from_text['entry_product_npr'] = $this->language->get('entry_new_price');
        $set_clear_from_text['entry_total'] = $this->language->get('entry_total_sum');
        $set_clear_from_text['entry_product_options'] = $this->language->get('entry_options');
        
        $set_tab['entry_add_set'] = $this->language->get('entry_add_set');
        $set_tab['entry_save_sets'] = $this->language->get('entry_save_sets');


        $set_clear_from = array_merge($set_clear_from, $set_clear_from_text);

        if (!empty($product_info['sets']))
        {
            $sets = json_decode($product_info['sets'], true);

            $sets_html = '';
            foreach ($sets as $key => $set)
            {

                foreach ($set['products'] as $pri => &$pr)
                    $pr['options'] = $this->getOptProduct($pr['product_id']);

                $set['key'] = $key;
                $datas = array_merge($set, $set_clear_from_text);
                $form = str_replace($newline_symbols, "", $this->load->view('module/sets/set_form.tpl', $datas));
                $sets_html .= $form;
            }
            $set_tab['sets'] = $sets_html;
        }

        $set_tab['token'] = $this->session->data['token'];
        $set_tab['set_row'] = str_replace($newline_symbols, "", $this->load->view('module/sets/set_row.tpl', array()));
        $set_tab['set_clear_form'] = str_replace($newline_symbols, "", $this->load->view('module/sets/set_clear_form.tpl', $set_clear_from));

        $result = str_replace($newline_symbols, "", $this->load->view('module/sets/set_tab.tpl', $set_tab));
        return $result;
    }

    public function optionsForms()
    {
        if ($this->request->post['options'])
        {
            echo $this->load->view('module/sets/set_product_opt.tpl', $this->request->post);
        }
    }

    public function getOptProduct($id)
    {
        //error_reporting(0);

        $options = array();
        $this->load->model('tool/image');
        $this->load->model('catalog/option');

        $product_options = $this->model_catalog_product->getProductOptions($id);

        foreach ($product_options as $product_option)
        {
            $option_info = $this->model_catalog_option->getOption($product_option['option_id']);

            if ($option_info)
            {
                $product_option_value_data = array();

                foreach ($product_option['product_option_value'] as $product_option_value)
                {
                    $option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);

                    if ($option_value_info)
                    {
                        $product_option_value_data[] = array(
                            'product_option_value_id' => $product_option_value['product_option_value_id'],
                            'option_value_id' => $product_option_value['option_value_id'],
                            'name' => $option_value_info['name'],
                            'price' => (float) $product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
                            'price_prefix' => $product_option_value['price_prefix']
                        );
                    }
                }

                $options[] = array(
                    'product_option_id' => $product_option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id' => $product_option['option_id'],
                    'name' => $option_info['name'],
                    'type' => $option_info['type'],
                    'value' => $product_option['value'],
                    'required' => $product_option['required']
                );
            }
        }
        return $options;
    }

    public function saveSets()
    {
        $this->load->language('module/sets');
        $json = array();

        if (!$this->user->hasPermission('modify', 'module/sets'))
        {
            $json['error'] = $this->language->get('error_permission');
        } else if (empty($this->request->post['product_id']))
        {
            $json['error'] = $this->language->get('error_new_prod');
        } else if (isset($this->request->post['set']) && is_array($this->request->post['set']) && count($this->request->post['set']))
        {
            foreach ($this->request->post['set'] as $s)
                if (count($s['products']) <= 1)
                    $json['error'] = $this->language->get('error_count_prod');
        }

        if (!isset($json['error']))
        {

            $sets = '';
            if (isset($this->request->post['set']))
                $sets = json_encode($this->request->post['set'], JSON_UNESCAPED_UNICODE);


            $this->db->query("UPDATE " . DB_PREFIX . "product SET sets = '" . $sets . "' WHERE product_id = '" . (int) $this->request->post['product_id'] . "'");

            $json['success'] = $this->language->get('entry_save');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function index()
    {
        $this->load->language('module/sets');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate())
        {
            $this->model_setting_setting->editSetting('sets', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], true));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_info'] = $this->language->get('text_info');
        $data['text_before'] = $this->language->get('text_before');
        $data['text_after'] = $this->language->get('text_after');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_status'] = $this->language->get('entry_status');

        $data['entry_show_disc_prec'] = $this->language->get('entry_show_disc_prec');
        $data['entry_show_old_price'] = $this->language->get('entry_show_old_price');
        $data['entry_show_qty'] = $this->language->get('entry_show_qty');

        $data['entry_position'] = $this->language->get('entry_position');
        $data['entry_selector'] = $this->language->get('entry_selector');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning']))
        {
            $data['error_warning'] = $this->error['warning'];
        } else
        {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/sets', 'token=' . $this->session->data['token'], true)
        );

        $data['action'] = $this->url->link('module/sets', 'token=' . $this->session->data['token'], true);

        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], true);

        if (isset($this->request->post['sets_show_disc_prec']))
            $data['sets_show_disc_prec'] = $this->request->post['sets_show_disc_prec'];
        else
            $data['sets_show_disc_prec'] = $this->config->get('sets_show_disc_prec');

        if (isset($this->request->post['sets_show_old_price']))
            $data['sets_show_old_price'] = $this->request->post['sets_show_old_price'];
        else
            $data['sets_show_old_price'] = $this->config->get('sets_show_old_price');

        if (isset($this->request->post['sets_show_qty']))
            $data['sets_show_qty'] = $this->request->post['sets_show_qty'];
        else
            $data['sets_show_qty'] = $this->config->get('sets_show_qty');

        if (isset($this->request->post['sets_status']))
            $data['sets_status'] = $this->request->post['sets_status'];
        else
            $data['sets_status'] = $this->config->get('sets_status');

        if (isset($this->request->post['sets_position']))
            $data['sets_position'] = $this->request->post['sets_position'];
        else
            $data['sets_position'] = $this->config->get('sets_position');

        if (isset($this->request->post['sets_selector']))
            $data['sets_selector'] = $this->request->post['sets_selector'];
        else if ($this->config->get('sets_selector'))
            $data['sets_selector'] = $this->config->get('sets_selector');
        else
            $data['sets_selector'] = '#content';


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/sets/sets.tpl', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/sets'))
        {
            $this->error['warning'] = $this->language->get('error_permission');
        }


        return !$this->error;
    }

}
