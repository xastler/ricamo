<?php
class ModelAccountWishlist extends Model {
	public function addWishlist($product_id) {
		$this->event->trigger('pre.wishlist.add');

		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$this->customer->getId() . "' AND product_id = '" . (int)$product_id . "'");

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_wishlist SET customer_id = '" . (int)$this->customer->getId() . "', product_id = '" . (int)$product_id . "', date_added = NOW()");

		$this->event->trigger('post.wishlist.add');
	}

	public function getShareWishlist($share_data = 1) {
        $data = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_share_wishlist WHERE share_data = '" . (string)$share_data . "'");
        if($query->num_rows){
            $data = json_decode($query->row['data'],true);
        }
        return $data;

    }
    
	public function shareWishlist($wishlist) {

        $share_hash = $this->config->generateHash(4);
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_share_wishlist SET share_hash = '" . $this->db->escape($share_hash) . "', data = '" . $this->db->escape(isset($wishlist) ? json_encode($wishlist) : '') . "', date_added = NOW()");

        $share_id = $this->db->getLastId();

        $share_data = $share_hash.$share_id;

        $this->db->query("UPDATE " . DB_PREFIX . "customer_share_wishlist SET share_data = '" . $this->db->escape($share_data) . "' WHERE share_id = '" . (int)$share_id . "'");

        return $share_data;
    
    }
    
	public function deleteWishlist($product_id) {
		$this->event->trigger('pre.wishlist.delete');

		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$this->customer->getId() . "' AND product_id = '" . (int)$product_id . "'");

		$this->event->trigger('post.wishlist.delete');
	}

	public function getWishlist() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->rows;
	}

	public function getTotalWishlist() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}
}
