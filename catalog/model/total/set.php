<?php

class ModelTotalSet extends Model
{

    public function getTotal(&$total_data, &$total, &$taxes)
    {

        if (isset($this->session->data['set']))
        {

            $this->load->language('total/set');


            if (!$this->validate())
                return false;

            $total_data[] = array(
                'code' => 'set',
                'title' => sprintf($this->language->get('text_set')),
                'value' => -$this->session->data['set']['discount'],
                'sort_order' => $this->config->get('set_sort_order')
            );

            $total -= $this->session->data['set']['discount'];
        }
    }

    public function validate()
    {
        $cps = $this->cart->getProducts();


        foreach ($this->session->data['set']['pinfo'] as $p)
        {
            foreach ($cps as $cp)
                if ((int) $p['id'] == (int) $cp['product_id'] && (int) $p['q'] <= (int) $cp['quantity'])
                    continue 2;

            return false;
        }
        return true;
    }

}
