<?php
class ControllerModuleCategoryHome extends Controller {
	public function index() {
		$this->load->language('module/category');

		$data['heading_title'] = $this->language->get('heading_title');
        $data['text_btn'] = $this->language->get('text_btn');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

        $this->load->model('tool/image');

		$data['categories'] = array();


        $this->load->model('catalog/information');
        $category_info = $this->model_catalog_category->getCategory($this->config->get('config_category'));
        $data['link_new'] = $this->url->link('product/category', 'path=' . $category_info['category_id']);

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach($children as $child) {
					$filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);


                    if ($child['image']) {
                        $image = $this->model_tool_image->resize($child['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                    }

                    $level_three_data = array();

                    $child_three = $this->model_catalog_category->getCategories($child['category_id']);
//                    var_dump($child_three);

                    foreach($child_three as $three) {

                        if ($three['image']) {
                            $image = $this->model_tool_image->resize($three['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                        } else {
                            $image = $this->model_tool_image->resize('', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                        }

                        $level_three_data[] = array(
                            'category_id' => $three['category_id'],
                            'name' => $three['name'] . ($this->config->get('config_product_count') ? ' <span>' . $this->model_catalog_product->getTotalProducts($filter_data) . '</span>' : ''),
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $three['category_id']),
                            'thumb' => $image
                        );
                    }

                    if ($child['category_img']) {
                        $children_data[] = array(
                            'category_id' => $child['category_id'],
                            'name' => $child['name'] . ($this->config->get('config_product_count') ? ' <span>' . $this->model_catalog_product->getTotalProducts($filter_data) . '</span>' : ''),
                            'level_three' => $level_three_data,
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
                            'thumb' => $image
                        );
                    }
				}


			$filter_data = array(
				'filter_category_id'  => $category['category_id'],
				'filter_sub_category' => true
			);

                $data['categories'][] = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'] . ($this->config->get('config_product_count') ? ' <span>' . $this->model_catalog_product->getTotalProducts($filter_data) . '</span>' : ''),
                    'children' => $children_data,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                );


		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category_home.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/category_home.tpl', $data);
		} else {
			return $this->load->view('default/template/module/category_home.tpl', $data);
		}
	}
}