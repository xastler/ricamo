<?php

class ControllerModuleSets extends Controller
{

    public function index()
    {
        
    }

    public function getSets()
    {
        if (!$this->config->get('sets_status'))
            return;

        $this->load->language('module/set');

        if (isset($this->request->get['product_id']))
            $product_id = $this->request->get['product_id'];
        else
            $product_id = 0;

        $data['text_sets'] = $this->language->get('text_sets');
        $data['buy_sets'] = $this->language->get('buy_sets');

        $product_info = $this->model_catalog_product->getProduct($product_id);
        $sets = false;

        if ($product_info)
            if (!empty($product_info['sets']))
            {
                $sets = json_decode($product_info['sets'], true);

                foreach ($sets as &$set)
                {
                    $js_array = array();
                    foreach ($set['products'] as &$product)
                    {
                        if (isset($product['use_option']) && isset($product['option']))
                            $option = $product['option'];
                        else
                            $option = array();

                        $js_array[] = array('id' => $product['product_id'], 'q' => $product['quantity'], 'option' => $option);
                        $pr_info = $this->model_catalog_product->getProduct($product['product_id']);
                        $product['href'] = $this->url->link('product/product', 'product_id=' . $product['product_id']);
                        if ($pr_info['image'])
                            $product['thumb'] = $this->model_tool_image->resize($pr_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
                        else
                            $product['thumb'] = false;
                        $product['discount_prec'] = floor(abs($product['old_price'] - $product['new_price']) / ($product['old_price'] / 100));
                        $product['old_price'] = $this->currency->format($this->tax->calculate($product['old_price'], $pr_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        $product['new_price'] = $this->currency->format($this->tax->calculate($product['new_price'], $pr_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    }
                    $disc_prec = floor(abs($set['old_summ'] - $set['new_summ']) / ($set['old_summ'] / 100));
                    $set['set_discount_fixed'] = $set['old_summ'] - $set['new_summ'];

                    $set['set_discount_prec'] = $disc_prec;
                    $set['old_summ'] = $this->currency->format($set['old_summ'], $this->session->data['currency']);
                    $set['new_summ'] = $this->currency->format($set['new_summ'], $this->session->data['currency']);
                    $set['js_array'] = json_encode($js_array);
                }
            }
        $data['sets'] = $sets;

        $data['show_disc_prec'] = $this->config->get('sets_show_disc_prec');
        $data['show_old_price'] = $this->config->get('sets_show_old_price');
        $data['show_qty'] = $this->config->get('sets_show_qty');

        $data['selector'] = $this->config->get('sets_selector');
        $data['position'] = $this->config->get('sets_position');

        return $this->load->view('default/template/module/sets.tpl', $data);
    }

    public function addSet()
    {
        $recurring_id = 0;
        $option = array();
        $json = array();

        if (isset($this->request->post['pinfo']) && isset($this->request->post['discount']))
        {
            $pinfo = $this->request->post['pinfo'];
            foreach ($pinfo as $p)
            {

                $option = (isset($p['option'])) ? $p['option'] : array();
                $this->cart->add($p['id'], $p['q'], $option, $recurring_id);
            }


            $this->session->data['set']['pinfo'] = $this->request->post['pinfo'];
            $this->session->data['set']['discount'] = $this->request->post['discount'];


            $json['redirect'] = str_replace('&amp;', '&', $this->url->link('checkout/cart'));
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
