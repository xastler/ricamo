<?php
class ControllerModuleCategory extends Controller {
	public function index() {
		$this->load->language('module/category');

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		if (isset($parts[2])) {
			$data['three_id'] = $parts[2];
		} else {
			$data['three_id'] = 0;
		}
        if (isset($parts[3])) {
            $data['three_li_id'] = $parts[3];
        } else {
            $data['three_li_id'] = 0;
        }

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

        $this->load->model('tool/image');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);
		foreach ($categories as $category) {
			$children_data = array();

            $children = $this->model_catalog_category->getCategories($category['category_id']);

            foreach($children as $child) {
                $filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

                $level_three_data = array();

                $child_three = $this->model_catalog_category->getCategories($child['category_id']);

                foreach($child_three as $three) {

                    $level_three_data2 = array();

                    $three_ul = $this->model_catalog_category->getCategories($three['category_id']);
                    foreach($three_ul as $three_li) {
                        $filter_data3 = array(
                            'filter_category_id'  => $three_li['category_id'],
                            'filter_sub_category' => true
                        );
                        $level_three_data2[] = array(
                            'category_id' => $three_li['category_id'],
                            'name' => $three_li['name'] . ($this->config->get('config_product_count') ? ' <span>' . $this->model_catalog_product->getTotalProducts($filter_data3) . '</span>' : ''),
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $three['category_id'] . '_' . $three_li['category_id'])
                        );
                    }
                    $filter_data2 = array(
                        'filter_category_id'  => $three['category_id'],
                        'filter_sub_category' => true
                    );
                    $level_three_data[] = array(
                        'category_id' => $three['category_id'],
                        'level_three'    => $level_three_data2,
                        'name' => $three['name'] . ($this->config->get('config_product_count') ? ' <span>' . $this->model_catalog_product->getTotalProducts($filter_data2) . '</span>' : ''),
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $three['category_id'])
                    );
                }

                if ($child['image'] && $child['category_icon']) {
                    $image = $this->model_tool_image->resize($child['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
                } else {
                    $image = $this->model_tool_image->resize(' ', $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
                }
                $filter_data1 = array(
                    'filter_category_id'  => $child['category_id'],
                    'filter_sub_category' => true
                );
                $children_data[] = array(
                    'category_id' => $child['category_id'],
                    'level_three'    => $level_three_data,
                    'name' => $child['name'] . ($this->config->get('config_product_count') ? ' <span>' . $this->model_catalog_product->getTotalProducts($filter_data1) . '</span>' : ''),
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
                    'thumb' => $image
                );

			}

			$filter_data = array(
				'filter_category_id'  => $category['category_id'],
				'filter_sub_category' => true
			);

            if ($category['image'] && $category['category_icon']) {
                $image0 = $this->model_tool_image->resize($category['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
            } else {
                $image0 = $this->model_tool_image->resize(' ', $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
            }


            $data['categories'][] = array(
                'category_id' => $category['category_id'],
                'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' <span>' . $this->model_catalog_product->getTotalProducts($filter_data) . '</span>' : ''),
                'thumb' => $image0,
                'children'    => $children_data,
                'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
            );

		}



        $this->load->model('catalog/manufacturer');
        $data['manufacturers'] = array();

        $results = $this->model_catalog_manufacturer->getManufacturers();

        foreach ($results as $result) {

            $data['manufacturers'][] = array(
                'name' => $result['name'],
                'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
            );
        }





        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
		} else {
			return $this->load->view('default/template/module/category.tpl', $data);
		}
	}
}