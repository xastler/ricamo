<?php
class ControllerPaymentCod2 extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/cod2.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/cod2.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/cod2.tpl', $data);
		}
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'cod2') {
			$this->load->model('checkout/order');

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('cod2_order_status_id'));

            $html = $this->model_checkout_order->invoice($this->session->data['order_id'], $this->config->get('cod_order_status_id'));

            require_once(DIR_SYSTEM."library/dompdf/dompdf_config.inc.php");
            $dompdf = new DOMPDF();
            $dompdf->load_html($html);
            $dompdf->render();
            $pdf = $dompdf->output();
            file_put_contents(DIR_DOWNLOAD . "invoice-".$this->session->data['order_uniqid'].".pdf", $pdf);
		}
	}
}
