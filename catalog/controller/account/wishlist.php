<?php
class ControllerAccountWishList extends Controller {
    public function index() {
        if (!$this->customer->isLogged()) {
            //$this->session->data['redirect'] = $this->url->link('account/wishlist', '', 'SSL');

            //$this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/locale/'.$this->session->data['language'].'.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->load->language('account/wishlist');

        $this->load->model('account/wishlist');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        if (isset($this->request->get['remove'])) {
            // Remove Wishlist
            $this->model_account_wishlist->deleteWishlist($this->request->get['remove']);

            if (!$this->customer->isLogged()) {
                unset($this->session->data['wishlist'][$this->request->get['remove']]);
            }

            $this->session->data['success'] = $this->language->get('text_remove');

            $this->response->redirect($this->url->link('account/wishlist'));
        }


        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/wishlist')
        );

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_empty'] = $this->language->get('text_empty');

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_stock'] = $this->language->get('column_stock');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_remove'] = $this->language->get('button_remove');

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['products'] = array();



        if ($this->customer->isLogged()) {
            $results = $this->model_account_wishlist->getWishlist();
        } else {
            if(!empty($this->session->data['wishlist'])){
                foreach ($this->session->data['wishlist'] as $res){
                    $results[] = array(
                        'product_id' => $res
                    );
                }
            } else {
                $results = array();
            }
        }

        $share_template = false;

        $title = $this->language->get('heading_title');

        if (isset($this->request->get['w'])) {

            $this->document->addOGMeta('property="og:description"', 'Перегляньте мій вішліст ');
            $share_template = true;
            $results = array();

            $this->load->language('product/category');

            $data['text_refine'] = $this->language->get('text_refine');
            $data['text_empty'] = $this->language->get('text_empty');
            $data['text_quantity'] = $this->language->get('text_quantity');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model'] = $this->language->get('text_model');
            $data['text_price'] = $this->language->get('text_price');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_points'] = $this->language->get('text_points');
            $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
            $data['text_sort'] = $this->language->get('text_sort');
            $data['text_limit'] = $this->language->get('text_limit');

            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_continue'] = $this->language->get('button_continue');
            $data['button_list'] = $this->language->get('button_list');
            $data['button_grid'] = $this->language->get('button_grid');

            $data['categories'] = array();
            $data['limits'] = array();
            $data['sorts'] = array();
            
            $data['description'] = '';
            $data['thumb'] = '';
            $data['compare'] = '';
            $data['button_list'] = '';
            
            $data['pagination'] = false;
            $data['results'] = false;
            
            $title = $data['heading_title'] = 'Вішліст';
            
            $share_wishlist = $this->model_account_wishlist->getShareWishlist($this->request->get['w']);
            foreach($share_wishlist as $item_list){
                    if($item_list['product_id']){
                        //$this->add($item_list['product_id']);
                        $results[] = array(
                            'product_id' => $item_list['product_id']
                        );
                    }
            }
        }

        $this->document->setTitle($title);

        foreach ($results as $result) {
            $product_info = $this->model_catalog_product->getProduct($result['product_id']);

            if ($product_info) {
                if(!$share_template){
                    if ($product_info['image']) {
                        $image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_wishlist_width'), $this->config->get('config_image_wishlist_height'));
                    } else {
                        $image = false;
                    }
                } else {
                    if ($product_info['image']) {
                        $image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                    }
                }

                if ($product_info['quantity'] <= 0) {
                    $stock = $product_info['stock_status'];
                } elseif ($this->config->get('config_stock_display')) {
                    $stock = $product_info['quantity'];
                } else {
                    $stock = $this->language->get('text_instock');
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$product_info['rating'];
                } else {
                    $rating = false;
                }

                $data['products'][] = array(
                    'product_id' => $product_info['product_id'],
                    'thumb'      => $image,
                    'name'       => $product_info['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'model'      => $product_info['model'],
                    'stock'      => $stock,
                    'price'      => $price,
                    'special'    => $special,
                    'tax'         => $tax,
                    'minimum'     => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
                    'rating'      => $rating,
                    'href'       => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                    'remove'     => $this->url->link('account/wishlist', 'remove=' . $product_info['product_id'])
                );
            } else {
                $this->model_account_wishlist->deleteWishlist($result['product_id']);
            }
        }


        $data['continue'] = $this->url->link('account/account', '', 'SSL');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if(!$share_template){
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/wishlist.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/wishlist.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/account/wishlist.tpl', $data));
            }
        } else {
            

            $data['column_right'] = false;;
            
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/category.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/product/category.tpl', $data));
            }
        }
    }

    public function share() {
        $json = array();
        $wishlist = array();
        $hash = '';

        $method = @$this->request->post['method'];
        $email = @$this->request->post['email'];

        $this->load->model('account/wishlist');

        if ($this->customer->isLogged()) {
            $wishlist = $this->model_account_wishlist->getWishlist();
        } else {
            if(!empty($this->session->data['wishlist'])){
                foreach ($this->session->data['wishlist'] as $res){
                    $wishlist[] = array(
                        'product_id' => $res
                    );
                }
            }
        }

        if($method == 'email'){
            if (( mb_strlen($email) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
                $json['email_error'] = 1;
                $wishlist = false;
            }
        }
        
        if($wishlist){
            $hash = $this->model_account_wishlist->shareWishlist($wishlist);
        }

        if($hash){
           $link = $json['link'] = $this->url->link('account/wishlist', 'w=' . $hash);
        }

        if(($method == 'email') && isset($link)){

            $subject = 'Вішлист з '.$_SERVER['HTTP_HOST'];
            $html = '';
            $html .= 'Рекомендований список товарів з  '.$_SERVER['HTTP_HOST'];
            $html .= ' <a href="'.$link.'">Перейти</a>'.'<br/>'. "\n\r";

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');


            $mail->setTo($email);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode('Поділитися вішлистом', ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($html);
            $mail->setText($html);
            $mail->send();
        
        } elseif(($method == 'fb') && isset($link)){
            $json['url_fb'] = $link;
        }
        
        $json['wishlist'] = $wishlist;

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        
    }
    
    public function add($get_product_id = false) {
        $this->load->language('account/wishlist');

        $json = array();

        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];
        } else {
            $product_id = ($get_product_id) ? $get_product_id : 0;
        }
        

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {
            if ($this->customer->isLogged()) {
                // Edit customers cart
                $this->load->model('account/wishlist');

                $this->model_account_wishlist->addWishlist($product_id);

                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . (int)$product_id), $product_info['name'], $this->url->link('account/wishlist'));

                $json['total'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
            } else {
                
                if (!isset($this->session->data['wishlist'])) {
                    $this->session->data['wishlist'] = array();
                }

                $this->session->data['wishlist'][$product_id] = $product_id;

                $this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);

                //$json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'), $this->url->link('product/product', 'product_id=' . (int)$product_id), $product_info['name'], $this->url->link('account/wishlist'));

                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . (int)$product_id), $product_info['name'], $this->url->link('account/wishlist'));

                $json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
            }
        }
        if(!$get_product_id){
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }
}
