<?php
// Heading 
$_['heading_title']     = 'НОВИНИ';

// Text
$_['text_headlines']    = 'Всі статті';
$_['text_comments']     = 'коментарі до цієї статті';
$_['text_comments_v']   = 'переглянути коментарі';
$_['button_more']       = 'Читати далі';
$_['text_posted_by']    = 'Опублікував';
$_['text_posted_on']    = 'На';
$_['text_posted_pon']   = 'Опубліковано';
$_['text_posted_in']    = 'Додано в';
$_['text_updated_on']   = 'Оновлено на';
$_['text_blogpage']     = 'Заголовки блогів';
?>
