<?php
// Text
$_['text_information']      =   'Магазин';
$_['text_service']          =   'Служба підтримки';
$_['text_extra']            =   'Додатково';
$_['text_contact']          =   'Контакти';
$_['text_return']           =   'Повернення товару';
$_['text_sitemap']          =   'Мапа сайту';
$_['text_manufacturer']     =   'Виробники';
$_['text_voucher']          =   'Подарункові сертифікати';
$_['text_affiliate']        =   'Партнери';
$_['text_special']          =   'Акції';
$_['text_account']          =   'Кабінет';
$_['text_cart']             =   'Кошик';
$_['text_order']            =   'Замовлення';
$_['text_wishlist']         =   'Список побажань';
$_['text_newsletter']       =   'Розсилка новин';
$_['text_powered']          =   '&copy;%s, %s ';
$_['text_testimonial']      =   'Відгуки';
$_['text_title_contact']    =   'Контакти';

$_['entry_name']      	    =   'Ім\'я';
$_['entry_phone']           =   'Номер мобільного телефону';
$_['text_call']      	    =   'Замовити дзвінок';
$_['text_send']      	    =   'Відправити';
$_['text_loading']          =   'Опрацювання';
$_['text-logo']             =   'Home';
