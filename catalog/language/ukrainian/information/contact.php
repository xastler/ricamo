<?php
// Heading
$_['heading_title']  = 'Контакти';

// Text
$_['text_location']  = 'Наша Адреса';
$_['text_store']     = 'Наші магазини';
$_['text_contact']   = 'Зворотній зв\'язок';
$_['text_address']   = 'Адреса';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Час роботи';
$_['text_comment']   = 'Коментар';
$_['text_button']   = 'Надіслати';
$_['text_success']   = '<p>Ваш запит був успішно відправлений адміністрації магазину!</p>';

// Entry
$_['entry_name']     = 'Ваше ім’я';
$_['entry_email']    = 'Ваш Email';
$_['entry_enquiry']  = 'Питання';

// Email
$_['email_subject']  = 'Повідомлення %s';

// Errors
$_['error_name']     = 'Ім’я має бути від 3 до 32 символів!';
$_['error_email']    = 'E-Mail вказано некоректно!';
$_['error_enquiry']  = 'Повідомлення має бути від 10 до 3000 символів!';
