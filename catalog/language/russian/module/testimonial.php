<?php
// Text
$_['heading_title']	           = 'Отзывы';

$_['text_write']               = 'Написать отзыв';
$_['text_login']               = 'Пожалуйста, <a href="%s"> Войдите </a> или <a href="%s"> зарегистрируйтесь</a>, чтобы просмотреть';
$_['text_no_reviews']          = 'Отзывов нет.';
$_['text_note']                = '<span class="text-danger">Примечание:</span> HTML не вписывать!';
$_['text_success']             = 'Спасибо за Ваш отзыв. Он был направлен на модерацию.';

$_['text_mail_subject']        = 'У вас есть новый отзыв (%s).';
$_['text_mail_waiting']	       = 'У вас есть новый отзыв.';
$_['text_mail_author']	       = 'Автор: %s';
$_['text_mail_rating']	       = 'Рейтинг: %s';
$_['text_mail_text']	       = 'Текст:';


$_['text_review_all']	       = 'Отзывы о магазине';
$_['text_review_prod']	       = 'Отзывы о товарах';



// Entry
$_['entry_name']               = 'Ваше имя';
$_['entry_review']             = 'Ваш отзыв';
$_['entry_rating']             = 'Ваша оценка';
$_['entry_good']               = 'Отправить';
$_['entry_bad']                = 'Плохо';

// Button
$_['button_continue']          = 'Отправить';

// Error
$_['error_name']                              = 'Имя должно быть от 2 до 25 символов!';
$_['error_email']          = 'E-Mail введён неправильно!';
$_['error_text']                              = 'Текст Отзыва должен быть от 25 до 1000 символов!';
$_['error_rating']                            = 'Пожалуйста поставьте оценку!';

