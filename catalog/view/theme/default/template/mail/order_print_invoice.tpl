<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru-ru" lang="ru-ru" dir="ltr">
<head>
    <base href="<?php echo $base;?>">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Podilsky Ricamo - Накладна</title>
    <link rel="stylesheet" type="text/css" href="view/theme/default/stylesheet/invoice.css?<?php echo time();?>" />
    <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>

    <script type="text/javascript">
        /*$(document).ready(function () {
            $('.print > a').on('click', function (event) {
                event.preventDefault();
                window.focus();
                window.print();
            });
        });*/
    </script>
</head>
<body class="contentpane">
    <div id="print_layout">
        <style type="text/css">
            body {
                margin: 0;
                padding: 0;
                background: #fff;
                font-size: 68.8%;
            }
            input:focus, select:focus, textarea:focus {
                background-color: #ffd;
            }
            * :focus, a:active {
                outline: none;
            }
            h1 {
                margin: 10px 0 5px 10px;
            }
            .print a {
                color: #FFF;
                font-size: 12px;
                padding: 6px 10px;
                background-color: #aaa588;
                text-decoration: none;
            }
            .print a:hover {
                background-color: #9A967B;
            }
            /* Print forms */
            #print_layout h1 input {color: #025A8D; font-size: 20px; font-weight: bold; border:none; width: 100%;}
            #print_layout .print {position: fixed; right: 20px; top: 15px; font-size: 13px;}
            #print_layout .buyer input {font-size:12px; font-weight: bold; border:none; width: 100%; text-align: center;}
            #print_layout table.hdr {border-collapse: collapse; width: 100%; border-top:2px solid #000;}
            #print_layout table.hdr tr:first-child td {padding-top:12px;}
            #print_layout table.hdr tr td {font-size:12px;}
            #print_layout table.hdr tr.buyer td {padding:5px 0;}
            #print_layout table.hdr tr td:first-child {padding-left:3px;}
            #print_layout table.hdr tr td:last-child {font-weight:bold; padding-left:8px;}
            #print_layout table.inv {border-collapse: collapse; width: 100%; border-top:2px solid #000;}
            #print_layout table.inv .item td, #print_layout table.inv thead th {border:2px solid #000;}
            #print_layout table.inv th:last-child,
            #print_layout table.inv .item td:last-child {border-right:2px solid #000;}
            #print_layout table.inv th:first-child, #print_layout table.inv .item td:first-child {border-left:2px solid #000;}
            #print_layout table.inv .total td {padding-top:8px; font-size:13px;}
            #print_layout table.inv .total.border-top td {    border-top: 2px solid #000;}
            #print_layout table.inv .itog td {font-size:13px;}
            #print_layout table.inv .prop td {padding:2px 0 8px; font-size:13px;}
            #print_layout table.inv .footer {border-top: 2px solid #000;}
            #print_layout table.inv .footer span {display: inline-block; min-width:40%; padding:12px 10px; font-size:13px; font-weight:bold;}
            #print_layout table.inv .footer .col-right {padding: 0; text-align: right;}
            #print_layout table.inv .footer .col-left {padding: 0; text-align: left}
            #print_layout table.fl {font-size:12px; border-collapse: collapse;    margin: 2px;}
            #print_layout table.fl .main {font-size:17px;}
            #print_layout table.fl .b2all {border: 2px solid #000;}
            #print_layout table.fl .b2top {border-top: 2px solid #000 !important;}
            #print_layout table.fl .item td {border: 1px solid #000; padding:1px 5px;}
            #print_layout table.fl .total td,
            #print_layout table.fl .itog td,
            #print_layout table.fl .prop td {padding:1px 5px;}
            #print_layout table.fl .item td:first-child {border: none; border-right: 2px solid #000 !important;}
            #print_layout table.fl .item td:last-child {border-right: 2px solid #000 !important;}
            #print_layout table.fl th {height:25px;}
            #print_layout table.fl .footer {display:none;}
            #print_layout table.fl input#jform_title {border:none; font-size:16px; text-align:center; padding-bottom: 1px;}

            @media print {
                #print_layout .print {display: none;}
            }
        </style>
        <?php //print_r($order);?>

        <h1><?php echo sprintf($header_title, $order_uniqid, $date_added);?></h1>
        <table class="hdr">
            <tbody>
            <tr>
                <td valign="top" rowspan="9">Постачальник:</td>
                <td>Іванов Іван Іванович</td>
            </tr>
            <tr>
                <td>ЄДРПОУ 1234567890</td>
            </tr>
            <tr>
                <td>Р/р 12345678901234 у ЗАТ КГРУ КБ "Приватбанк" МФО 123456</td>
            </tr>
            <tr>
                <td>Ід.номер 1234567890</td>
            </tr>
            <tr>
                <td>Є платником єдиного податку</td>
            </tr>
            <tr>
                <td>Адреса: 29000, м. Хмельницький вул. Свободи 5, оф.652</td>
            </tr>
            <tr>
                <td>+38 097 672 8511</td>
            </tr>
            <tr>
                <td>+38 097 672 8512</td>
            </tr>
            <tr>
                <td>+38 097 672 8513</td>
            </tr>
            <tr class="buyer">
                <td valign="top">Покупець:</td>
                <td align="left">
                    <?php echo $customer_name?>
                </td>
            </tr>
            </tbody>
        </table>

        <table class="inv">
            <thead>
            <tr>
                <th>№</th>
                <th colspan="2">Товар</th>
                <th>Кіл-ть</th>
                <th>Ціна</th>
                <th>Сума</th>
            </tr>
            </thead>
            <tbody>

            <?php

            $payment = 0;
            foreach ($totals as $total) {
                if($total['code'] == 'payment') {
                    $payment = $total['value'];
                }
            }

            $for_product = 0;
            if($payment) {
                $for_product = $payment / count($products);
            }

            //echo $for_product;
            ?>
            <?php $total_sum=0;$ind=0;foreach ($products as $product) { $ind++; ?>
                <tr class="item">
                    <td align="center"><?php echo $ind;?></td>
                    <td align="left" style="padding-left:10px;" colspan="2"><?php echo $product['name']; ?>
                        <?php foreach ($product['option'] as $option) { ?>
                            <br />
                            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                        <?php } ?></td>
                    <td align="center"><?php echo $product['quantity']; ?></td>
                    <?php
                    $pprice = str_replace(array(' ',','), array('','.'),$product['price']);
                    $ptotal = str_replace(array(' ',','), array('','.'),$product['total']);
                    ?>
                    <td align="center"><?php echo $product['price']; ?></td>
                    <td align="center"><?php echo $product['total'] ?></td>
                    <?php
                    $total_sum += round($ptotal + $for_product, 2);
                    ?>
                </tr>
            <?php } ?>

            <?php $k=0;$summ = 0;foreach ($totals as $total) {
            if ($total['code']== 'prepayment'){
                $summ = $total['value'] * -1;
            }
            if( $total['code']== 'payment' || $total['code']== 'prepayment' || $total['code']== 'sub_total') continue;  $k++;?>
        <tr class="total<?php echo ($k == 1)? ' border-top':''?><?php echo ' x_' .$total['code']?>">
            <td align="right" colspan="5"><b><?php echo ${'text_' . $total['code']}; ?>:</b></td>
            <td align="center"><b><?php echo number_format($total['value'] + $summ,2); ?></b></td>
        </tr>
        <?php } ?>
            <?php $k=1;$summ = 0;foreach ($totals as $total) {
                if($total['code'] == 'total'){
                    $summ=$total['value'];  ?>
                    <tr class="total<?php echo ($k == 1)? ' border-top':''?><?php echo ' x_' .$total['code']?>">
                        <td align="right" colspan="5"><b><?php echo ${'text_' . $total['code']}; ?>:</b></td>
                        <td align="center"><b><?php echo number_format($total['value'],2); ?></b></td>
                    </tr>
                <?php }} ?>
            <tr class="itog">
                <td colspan="6">Всього найменувань <?php echo $ind;?> на суму <?php echo number_format($summ,2); ?> гривень</td>
            </tr>
            <?php //echo $total_sum;?>
            <tr class="footer">
                <td colspan="3" class="col-left"><span>Відпустив_______________</span></td>
                <td colspan="3" class="col-right"><span>Отримав_______________</span></td>
            </tr>
            </tbody>
        </table>
    </div>
</body>
</html>
