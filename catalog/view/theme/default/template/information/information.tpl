<?php echo $header; ?>
    <div class="container ">
        <div class="row">
            <?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-md-9 col-sm-12 '; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div  class="<?php echo $class; ?>">
                <div id="content">
                    <?php echo $content_top; ?>
                    <h1><?php echo $heading_title; ?></h1>
                    <?php echo $description; ?>
                    <?php echo $content_bottom; ?>
                </div>
            </div>
            <?php echo $column_right; ?>
        </div>
    </div>
    <div class="sing-left"></div>
<?php echo $footer; ?>

<script>
    var jsVar = "<?= $register; ?>";
    $('#registe_opt').attr('href',jsVar);
</script>
