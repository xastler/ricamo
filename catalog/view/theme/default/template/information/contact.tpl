<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9 col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <div class="page-contacts">
          <div class="contacts-content">
              <div class="contacts-info">
                  <div class="contacts-telephones">
                      <?php if ($data['telephone1']) { ?>
                          <div class="contacts-telephone">
                              <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>"><i class="icon-phone"></i><?php echo $telephone1; ?></a>
                          </div>
                      <?php }?>
                      <?php if ($data['telephone2']) { ?>
                          <div class="contacts-telephone">
                              <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>"><i class="icon-phone"></i><?php echo $telephone2; ?></a>
                          </div>
                      <?php }?>
                      <?php if ($data['telephone3']) { ?>
                          <div class="contacts-telephone">
                              <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>"><i class="icon-phone"></i><?php echo $telephone3; ?></a>
                          </div>
                      <?php }?>
                  </div>
                  <?php if ($data['email']) { ?>
                      <div class="contacts-mail">
                          <i class="icon-icon-mail"></i>
                          <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                      </div>
                  <?php }?>
                  <?php if ($data['address']) { ?>
                      <div class="contacts-address">
                          <div class="contacts-title"><i class="icon-icon-map"></i>Наша адреса:</div>
                          <p><?php echo $address; ?></p>
                      </div>

                  <?php }?>
              </div>
              <div class="contacts-title">Слідкуйте за нами в соц. мережах:</div>
              <div class="contacts-social">
                <a href="<?php echo $facebook; ?>" target="_blank" class="icon-fb"></a>
                <a href="<?php echo $instagram; ?>" target="_blank" class="icon-insta"></a>
                <a href="<?php echo $youtube; ?>" target="_blank" class="icon-yt"></a>
              </div>
          </div>
          <div class="contacts-map">
              <div id="map" data-point="<?php echo $data['map-point']; ?>">

              </div>
              <script>

                  if($('#map').length){
                      init($('#map').data('point'));
                  }

                  function init(point) {
                      var point_y = point.slice(0, point.indexOf(','));
                      var point_x = point.slice(point.indexOf(' '),point.length);
                      console.log(point_x);
                      var mapOptions = {
                          zoom: 11,
                          center: new google.maps.LatLng(point_y, point_x)
                      };
                      var mapElement = document.getElementById('map');
                      var map = new google.maps.Map(mapElement, mapOptions);
                      var marker = new google.maps.Marker({
                          position: new google.maps.LatLng(point_y, point_x),
                          map: map,
                          title: 'Augusta Apartments\n' +
                              'Eichstrasse 6\n' +
                              '76530 Baden-Baden'
                      });
                  }
              </script>
          </div>
      </div>
      <?php if ($locations) { ?>
      <h3><?php echo $text_store; ?></h3>
      <div class="panel-group" id="accordion">
        <?php foreach ($locations as $location) { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><a href="#collapse-location<?php echo $location['location_id']; ?>" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $location['name']; ?> <i class="fa fa-caret-down"></i></a></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-location<?php echo $location['location_id']; ?>">
            <div class="panel-body">
              <div class="row">
                <?php if ($location['image']) { ?>
                <div class="col-sm-3"><img src="<?php echo $location['image']; ?>" alt="<?php echo $location['name']; ?>" title="<?php echo $location['name']; ?>" class="img-thumbnail" /></div>
                <?php } ?>
                <div class="col-sm-3"><strong><?php echo $location['name']; ?></strong><br />
                  <address>
                  <?php echo $location['address']; ?>
                  </address>
                  <?php if ($location['geocode']) { ?>
                  <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
                  <?php } ?>
                </div>
                <div class="col-sm-3"> <strong><?php echo $text_telephone; ?></strong><br>
                  <?php echo $location['telephone']; ?><br />
                  <br />
                  <?php if ($location['fax']) { ?>
                  <strong><?php echo $text_fax; ?></strong><br>
                  <?php echo $location['fax']; ?>
                  <?php } ?>
                </div>
                <div class="col-sm-3">
                  <?php if ($location['open']) { ?>
                  <strong><?php echo $text_open; ?></strong><br />
                  <?php echo $location['open']; ?><br />
                  <br />
                  <?php } ?>
                  <?php if ($location['comment']) { ?>
                  <strong><?php echo $text_comment; ?></strong><br />
                  <?php echo $location['comment']; ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
            <legend><?php echo $text_contact; ?></legend>
            <div class="block-form">
                <p>Напишіть своє питання. Наш менеджер зв'яжеться з вами протягом години <br>і допоможе вирішити вашу проблему</p>
                <div class="block-form-inputs">
                    <div class="block-form-input">
                        <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                        <div>
                            <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
                            <?php if ($error_name) { ?>
                                <div class="text-danger"><?php echo $error_name; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="block-form-input">
                        <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                        <div>
                            <input type="text" name="email" value="<?php //echo $email; ?>" id="input-email" class="form-control" />
                            <?php if ($error_email) { ?>
                                <div class="text-danger"><?php echo $error_email; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="block-form-textarea">
                    <label class="control-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
                    <div class="">
                        <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
                        <?php if ($error_enquiry) { ?>
                            <div class="text-danger"><?php echo $error_enquiry; ?></div>
                        <?php } ?>
                    </div>
                </div>
                <div class="block-form-btn">
                    <input type="submit" value="<?php echo $text_button; ?>" />
                </div>
            </div>
          <?php echo $captcha; ?>

        </fieldset>

      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
