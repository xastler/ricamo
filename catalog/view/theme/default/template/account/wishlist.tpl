<?php echo $header; ?>
<div class="container">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
      <script>
          $('header').append($('.alert.alert-success'));
          setTimeout(function () {
              $('.alert.alert-success').remove();
          }, 2100);
      </script>
  <?php } ?>

        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <h3><?php echo $heading_title; ?></h3>
            <?php if ($products) { ?>
            <div class="row">
            <?php foreach ($products as $product) { ?>
            <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="product-thumb transition">
              <div class="image">
                  <a href="<?php echo $product['href']; ?>">
                      <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                           title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
              <div class="caption">
                  <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                  <p><?php echo $product['description']; ?></p>
                  <?php if ($product['rating']) { ?>
                      <div class="rating">
                          <?php for ($i = 1; $i <= 5; $i++) { ?>
                              <?php if ($product['rating'] < $i) { ?>
                                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                              <?php } else { ?>
                                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                              <?php } ?>
                          <?php } ?>
                      </div>
                  <?php } ?>
                  <?php if ($product['price']) { ?>
                      <p class="price">
                          <?php if (!$product['special']) { ?>
                              <?php echo $product['price']; ?>
                          <?php } else { ?>
                              <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                          <?php } ?>
                          <?php if ($product['tax']) { ?>
                              <!--<span class="price-tax"><?php //echo $text_tax; ?> <?php //echo $product['tax']; ?></span>-->
                          <?php } ?>
                      </p>
                  <?php } ?>
              </div>
              <div class="button-group">
                  <button type="button" class="btn-add" onclick="cart.add('<?php echo $product['product_id']; ?>');"><span class=""><?php echo $button_cart; ?></span></button>

                  <button type="button" class="btn-icon" data-toggle="tooltip" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="icon-comparison"></i></button>
              </div>
              <a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn-wishlist-close"><i class="fa fa-times"></i></a></td>
            </div>
            </div>
            <?php } ?>
            </div>

                <div class="row">
                    <div  class=" col-sm-12">
                        <h2>Поділитися вішлистом</h2>
                        <div class="share-wishlist-content">
                            <div class="buttons clearfix">
                                <a href="javascript:void(0);" data-method="fb" class="icon-fb _share_wishlist"></a>
                            </div>
                            <form class="share-wishlist" action="#" onsubmit="return false;">
                                <div class="form-group">
    <!--                                <label for="email">Email address:</label>-->
                                    <input type="email" name='email' class="_share_wishlist_email" placeholder="Введіть Email">
                                </div>
    <!--                            <button type="submit" data-method="email" class="btn btn-warning _share_wishlist">Поділитися через Email</button>-->
                                <button type="submit" data-method="email" class="_share_wishlist">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="37" height="37">
                                        <path class="sircle" fill="#F65314" fill-rule="evenodd" d="M17.999-.001c9.941 0 18 8.059 18 18.001 0 9.941-8.059 17.999-18 17.999s-18-8.058-18-17.999c0-9.942 8.059-18.001 18-18.001z"></path>
                                        <path fill="none" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.998 18h16M19.999 24.999l6-6.999-6-7"></path>
                                    </svg>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>

            <?php } else { ?>
            <p><?php echo $text_empty; ?></p>
            <?php } ?>

            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
</div>
<?php echo $footer; ?>
