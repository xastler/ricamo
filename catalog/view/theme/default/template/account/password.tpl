<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
      <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
          <div class="acount-content">
              <div class="row">
                  <div class="col-nd-12">
                      <div class="account_title">
                          <h1><?php echo $text_account; ?></h1>
                      </div>
                  </div>
                  <div class="col-md-3">

                      <ul class="account_left_bar">
                          <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil; ?></a></li>
                          <li class="active"><a href="<?php echo $link_password; ?>"><?php echo $text_password; ?></a></li>
                          <li><a href="<?php echo $link_history; ?>"><?php echo $text_history; ?></a></li>
                          <li><a href="<?php echo $link_logout; ?>"><?php echo $text_logout; ?></a></li>
                      </ul>
                  </div>
                  <div class="col-md-9 col-sm-12">
                      <div class="wrapper-margin">
                          <div class="account_title">
                              <h2><?php echo $heading_title; ?></h2>
                          </div>
                          <form action="<?php echo $action; ?>" method="post" id="password_form" enctype="multipart/form-data" class="form-horizontal">
                              <div class="row">
                                  <div class="form-group required">
                                      <div class="col-sm-6">
                                          <div class="old_password">
                                              <input type="password" name="old_password" value="<?php echo $old_password; ?>" placeholder="<?php echo $entry_old_password; ?>" id="input-old_password" class="form-control input-ico" />
                                              <?php if ($error_old_password) { ?>
                                                  <div class="text-danger"><?php echo $error_old_password; ?></div>
                                              <?php } ?>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="form-group required">
                                      <div class="col-sm-6 margin-input">
                                          <div class="input-password">
                                              <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control input-ico" />
                                              <?php if ($error_password) { ?>
                                                  <div class="text-danger"><?php echo $error_password; ?></div>
                                              <?php } ?>
                                          </div>
                                      </div>
                                      <div class="col-sm-6">
                                          <div class="input-confirm">
                                              <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control input-ico" />
                                              <?php if ($error_confirm) { ?>
                                                  <div class="text-danger"><?php echo $error_confirm; ?></div>
                                              <?php } ?>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                          </form>
                          <div class="buttons clearfix">
                              <div class="pull-left">
                                  <input type="submit" form="password_form" value="<?php echo $button_save; ?>" class="btn btn-default" />
                              </div>
                          </div>
                      </div>

                  </div>
              </div>
          </div>

          <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>