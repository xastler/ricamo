<?php echo $header; ?>
<div class="container">

  <div class="row">
      <?php if ($success) { ?>
          <div class="alert alert-success"> <?php echo $success; ?><button type="button" class="close" data-dismiss="alert">&times;</button></div>
      <?php } ?>

      <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'ct-center-6'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">

            <?php echo $content_top; ?>
            <h1 class="hidden"><?php echo $heading_title; ?></h1>

            <div class="wrapper-margin">
                <p><?php echo $text_email; ?></p>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class=" required">
                                <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                        <?php if ($error_warning) { ?>
                            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                        <?php } ?>
                    </div>
                    <div class="buttons clearfix">
                        <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                        <div class="pull-right">
                            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                        </div>
                    </div>
                </form>
            </div>

            <?php echo $content_bottom; ?>

    </div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>