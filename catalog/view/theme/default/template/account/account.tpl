<?php echo $header; ?>
<?php if ($success) { ?>
    <script>
        $(function() {
            $('#xx-tovars .modal-top').html('<div class="alert"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>');
            $('.xsg').click();

        });
    </script>
<?php } ?>
<span class="xsg" data-toggle="modal" data-target=".bs-example-modal-sm"></span>

<div class="container">

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = '    col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

            <div class="acount-content">
                <div class="account_title">
                    <h1><?php echo $text_account; ?></h1>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <ul class="account_left_bar">
                            <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil; ?></a></li>
                            <li><a href="<?php echo $link_password; ?>"><?php echo $text_password; ?></a></li>
                            <li><a href="<?php echo $link_history; ?>"><?php echo $text_history; ?></a></li>
                            <li><a href="<?php echo $link_logout; ?>"><?php echo $text_logout; ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>
