<div class="top-bar-search" id="search">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>"/>
  <button type="button"><i class="fa fa-search"></i></button>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#search input[name="search"]').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: 'index.php?route=product/search/autocomplete&filter_name=' +  encodeURIComponent(request),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item.name,
                                value: item.href,
                                href: item.href
                            }
                        }));
                    }
                });
            },
            minLength: 3,
            select: function(ui,event) {
                //console.log(ui.href);
                if (ui.href == "") {
                    return false;
                } else {
                    //location.href = ui.href;
                    $('#search input[name="search"]').val(ui.label);
                    return false;
                }
            }
        });
    });
</script>