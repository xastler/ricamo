<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<?php foreach ($ogmeta as $meta_tag) { ?>
<meta <?php echo $meta_tag['meta_name']; ?> content="<?php echo $meta_tag['content']; ?>" >
<?php } ?>
    <?php if($gtm) { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?
id='+i+dl;f.parentNode.insertBefore(j,f);
 })(window,document,'script','dataLayer','<?php echo $gtm; ?>');</script>
<!-- End Google Tag Manager -->
    <?php } ?>
<link href="catalog/view/javascript/jquery/search/jquery-ui.autocomplete.css" rel="stylesheet">
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>

<script src="catalog/view/javascript/jquery/jquery.pjax.js" type="text/javascript"></script>

<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/style-main.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/main_y.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<script src="catalog/view/javascript/jquery/slick/slick.js" type="text/javascript"></script>

<link href="catalog/view/javascript/flipclock/flipclock.css" rel="stylesheet">
<script src="catalog/view/javascript/flipclock/flipclock.min.js" type="text/javascript"></script>

<link href="catalog/view/javascript/jquery/slick/slick.css" rel="stylesheet">
<link href="catalog/view/javascript/jquery/selectric/src/selectric.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="catalog/view/javascript/jquery/search/jquery-ui.autocomplete.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/zoomsl-3.0.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/selectric/src/jquery.selectric.js" type="text/javascript"></script>
<script src="catalog/view/javascript/main.js" type="text/javascript"></script>

<link href="catalog/view/fonts-icon/font.css" rel="stylesheet" type="text/css"/>
<!--<link href="catalog/view/fonts/font.css" rel="stylesheet" type="text/css" />-->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEi_mKbOqvmIL0qXYZEwCZujvacMpBxGQ&callback" type="text/javascript"></script>

    <?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link href="catalog/view/theme/default/stylesheet/sergo.css" rel="stylesheet">
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<?php if($gtm) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>>"
                      height="0" width="0"
                      style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>
<div class="panel-menu">
    <div class="panel-menu-top">
        <div class="panel-menu-close"><i class="fa fa-times"></i></div>
    </div>
    <div class="panel-menu-list">
        <ul>
            <?php if ($informations) { ?>
                <?php foreach ($informations as $information) { ?>
                    <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                <?php } ?>
            <?php } ?>
            <li><a href="<?php echo $testimonial; ?>"><?php echo $text_testimonial; ?></a></li>
            <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
            <?php if ($ncategories) { ?>
                <?php foreach ($ncategories as $ncat) { ?>
                    <li><a href="<?php echo $ncat['href']; ?>"><?php echo $ncat['name']; ?></a></li>
                <?php } ?>
            <?php } ?>
            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?> </a></li>
        </ul>
    </div>
    <div class="panel-menu-bottom">
        <ul>
            <li><a href="<?php echo $facebook; ?>" target="_blank" class="icon-fb"></a></li>
            <li><a href="<?php echo $instagram; ?>" target="_blank" class="icon-insta"></a></li>
            <li><a href="<?php echo $youtube; ?>" target="_blank" class="icon-yt"></a></li>
        </ul>
    </div>
</div>
<nav id="top">
  <div class="container">
    <div class="top-bar">
      <div class="top-bar-logo" id="logo">
          <?php if ($logo) { ?>
          <?php if ($home == $og_url) { ?>
          <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
          <?php } else { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } ?>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
      </div>
      <?php echo $search; ?>
      <div class="top-bar-navigation">
        <div class="top-bar-container top-bar-like <?php echo ($text_wishlist > 0)? ' has_product':''?>">
          <a href="<?php echo $wishlist; ?>"  class="icon-heart">
              <span class="num-select"><?php echo $text_wishlist; ?></span>
          </a>
        </div>
        <div class="top-bar-container top-bar-comparison <?php echo ($text_compare > 0)? ' has_product':''?>">
          <a href="<?php echo $compare ; ?>" class="icon-comparison">
              <span class="num-select"><?php echo $text_compare; ?></span>
          </a>
        </div>
        <div class="top-bar-cart" id="cart">
            <a href="<?php echo $shopping_cart; ?>" class="icon-cart">
                <span id="cart-total" class="num-select"><?php echo $text_items; ?></span>
            </a>
        </div>
        <div class="top-bar-account">
            <?php if ($logged) { ?>
                <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="icon-account-hover"></a>
            <?php } else{ ?>
                <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="icon-account"></a>
            <?php } ?>
        </div>
      </div>
    </div>
  </div>
</nav>

<header>
  <div class="container">
    <div class="header">
      <nav class="header-menu">
        <div class="header-menu-btn icon-list-1"></div>

        <ul>
            <?php if ($informations) { ?>
                <?php $k=''; foreach ($informations as $information) { $k++; ?>
                    <?php if ($k == 3) { ?>
                        <?php if ($testimonial == $og_url) { ?>
                            <li class="active"><a href="<?php echo $testimonial; ?>"><?php echo $text_testimonial; ?></a></li>
                        <?php } else{ ?>
                            <li><a href="<?php echo $testimonial; ?>"><?php echo $text_testimonial; ?></a></li>
                        <?php } ?>
                        <?php if ($information['href'] == $og_url) { ?>
                            <li class="active"><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } else{ ?>
                            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if ($information['href'] == $og_url) { ?>
                            <li class="active"><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } else{ ?>
                            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            <?php if ($special == $og_url) { ?>
                <li class="active"><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
            <?php } else{ ?>
                <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
            <?php } ?>
            <?php if ($ncategories) { ?>
                <?php foreach ($ncategories as $ncat) { ?>
                    <?php if ($ncat['href'] == $og_url) { ?>
                        <li class="active"><a href="<?php echo $ncat['href']; ?>"><?php echo $ncat['name']; ?></a></li>
                    <?php } else{ ?>
                        <li><a href="<?php echo $ncat['href']; ?>"><?php echo $ncat['name']; ?></a></li>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            <?php if ($contact == $og_url) { ?>
                <li class="active"><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?> </a></li>
            <?php } else{ ?>
                <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?> </a></li>
            <?php } ?>

        </ul>
      </nav>
      <div class="header-navigation_media">

      </div>
      <nav class="header-social">
        <ul>
          <li><a href="<?php echo $facebook; ?>" target="_blank" class="icon-fb"></a></li>
          <li><a href="<?php echo $instagram; ?>" target="_blank" class="icon-insta"></a></li>
          <li><a href="<?php echo $youtube; ?>" target="_blank" class="icon-yt"></a></li>
        </ul>
      </nav>
      <div class="header-phone">
        <span class="icon-phone"></span>
        <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>"><?php echo $telephone1; ?></a>
        <span class="icon-arrow"></span>
        <nav class="header-phone-nav">
          <ul>
            <li><span class="icon-phone"></span><a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>"><?php echo $telephone2; ?></a></li>
            <li><span class="icon-phone"></span><a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>"><?php echo $telephone3; ?></a></li>
            <li class="header-phone-media"><span class="icon-phone"></span><a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>"><?php echo $telephone1; ?></a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>
<div class="wrapper">
    <div class="wrapper-top">
        <div class="sing-right"></div>