</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="footer-logo">
                    <?php if ($logo) { ?>
                        <?php if ($home == $og_url) { ?>
                            <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                        <?php } else { ?>
                            <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                        <?php } ?>
                    <?php } else { ?>
                        <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                    <?php } ?>
                </div>
                <div class="footer-powered">
                    <?php echo $powered; ?>
                </div>
            </div>
            <div class="col-lg-2 col-md-none">
                <h5><?php echo $text_information; ?></h5>
                <ul class="list-unstyled">
                    <?php if ($informations) { ?>
                        <?php $k=''; foreach ($informations as $information) { $k++; ?>
                            <?php if ($k == 3) { ?>
                                <li><a href="<?php echo $testimonial; ?>"><?php echo $text_testimonial; ?></a></li>
                                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                            <?php } else { ?>
                                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </ul>

            </div>
            <div class="col-lg-2 col-md-3 col-sm-none">
                <h5><?php echo $text_account; ?></h5>
                <ul class="list-unstyled">
                    <li><a href="<?php echo $cart; ?>"><?php echo $text_cart; ?></a></li>
                    <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                    <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-none">
                <h5><?php echo $text_title_contact; ?></h5>
                <ul class="list-unstyled">
                    <li><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></li>
                    <li><a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>"><?php echo $telephone1; ?></a></li>
                    <li><a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>"><?php echo $telephone2; ?></a></li>
                    <li><a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>"><?php echo $telephone3; ?></a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <?php echo $newsletters ?>
                <nav class="footer-social">
                    <ul>
                        <li><a href="<?php echo $facebook; ?>" target="_blank" class="icon-fb"></a></li>
                        <li><a href="<?php echo $instagram; ?>" target="_blank" class="icon-insta"></a></li>
                        <li><a href="<?php echo $youtube; ?>" target="_blank" class="icon-yt"></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</footer>
<div id='xx-tovars' class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">

        <div class="modal-content">
            <div class="modal-top">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" style="z-index: 1041;" id="modal_popup_product" role="dialog">
    <div class="modal-dialog modal-lg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <!-- Modal content-->
        <div class="modal-content">

        </div>

    </div>
</div>
<style>
.loaderrr {
  position: relative;
}

.loaderrr::after {
  background-color: rgba(0,0,0,.7);
  content: 'Loading...';
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
}
</style>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>
