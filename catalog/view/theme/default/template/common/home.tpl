<?php echo $header; ?>


            <div class="container">
                <div class="row"><?php echo $column_left; ?>
                    <?php if ($column_left && $column_right) { ?>
                        <?php $class = 'col-sm-6'; ?>
                    <?php } elseif ($column_left || $column_right) { ?>
                        <?php $class = 'col-md-9 col-sm-12'; ?>
                    <?php } else { ?>
                        <?php $class = 'col-sm-12'; ?>
                    <?php } ?>
                    <div id="content"
                         class="<?php echo $class; ?>">
                        <?php echo $content_top; ?>


                    </div>
                    <?php echo $column_right; ?>
                </div>
            </div>
            <div class="sing-left"></div>
        </div>
        <div class="wrapper-bottom">
            <div class="container">
                <?php echo $content_bottom; ?>
            </div>
        </div>

<?php echo $footer; ?>