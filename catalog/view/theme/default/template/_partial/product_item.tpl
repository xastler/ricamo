<div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-6">
    <div class="product-thumb">
        <div class="image"><a data-product_id="<?=$product['product_id']?>" href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
        <div>
            <div class="caption">
                <a href="<?php echo $product['href']; ?>">
                    <h4><?php echo $product['name']; ?></h4>
                    <p><?php echo $product['description']; ?></p>
                </a>
                <?php if ($product['rating'] && 0) { ?>
                    <div class="rating">
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                            <?php if ($product['rating'] < $i) { ?>
                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <?php } else { ?>
                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                            <?php } ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($product['price']) { ?>
                    <p class="price">
                        <?php if (!$product['special']) { ?>
                            <?php echo $product['price']; ?>
                        <?php } else { ?>
                            <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                        <?php } ?>
                        <?php if ($product['tax']) { ?>
<!--                            <span class="price-tax">--><?php //echo $text_tax; ?><!-- --><?php //echo $product['tax']; ?><!--</span>-->
                        <?php } ?>
                    </p>
                <?php } ?>
            </div>
            <div class="button-group">
                <button data-toggle="modal" data-target=".bs-example-modal-sm" type="button" class="btn-add" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class=""><?php echo $button_cart; ?></span></button>
                <button data-toggle="modal" data-target=".bs-example-modal-sm" type="button" class="btn-icon" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-heart"></i></button>
                <button data-toggle="modal" data-target=".bs-example-modal-sm" type="button" class="btn-icon" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="icon-comparison"></i></button>
            </div>
        </div>
    </div>
</div>
