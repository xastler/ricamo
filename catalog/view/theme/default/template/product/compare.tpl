<?php echo $header; ?>
<div class="container">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="acount-content">
            <h1><?php echo $heading_title; ?></h1>
            <?php if ($products) { ?>
                <table class="table table-bordered table-compare">
                    <tbody>
                    <tr>
                        <td><?php echo $text_name; ?></td>
                        <?php foreach ($products as $product) { ?>
                            <td class="text-center">
                                <?php if ($product['thumb']) { ?>
                                    <div class="wrapper-img-compare">
                                        <a href="<?php echo $product['remove']; ?>" class="btn-wishlist-close"><i class="fa fa-times"></i></a>
                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="compare__img" />
                                        <a class="compare__name" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    </div>

                                <?php } ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td>
                            <div>
                                Код:
                            </div>
                            <div>
                                Категорія:
                            </div>
                            <div>
                                <?php echo $text_availability; ?>
                            </div>
                        </td>
                            <?php foreach ($products as $product) { ?>
                                <td>
                                    <?php if($product['model']) { ?>
                                        <div><?php echo $product['model']; ?></div>
                                    <?php } else { ?>
                                        <div> - </div>
                                    <?php }?>
                                    <?php if($product['category_title']) { ?>
                                        <div><?php echo $product['category_title']; ?></div>
                                    <?php } else { ?>
                                        <div> - </div>
                                    <?php }?>
                                    <?php if($product['availability']) { ?>
                                        <div class="green-compare"><?php echo $product['availability']; ?></div>
                                    <?php } else { ?>
                                        <div> - </div>
                                    <?php }?>
                                </td>
                            <?php } ?>
                    </tr>
                    <tr>
                        <td><?php echo $text_price; ?></td>
                        <?php foreach ($products as $product) { ?>
                        <td>
                            <?php if (!$product['options']) { ?>
                                <?php if ($product['price']) { ?>
                                    <?php if (!$product['special']) { ?>
                                        <?php echo $product['price']; ?>
                                    <?php } else { ?>
                                        <strike><?php echo $product['price']; ?></strike> <?php echo $product['special']; ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($product['options']) { ?>
                                <div class="option">
                                    <?php foreach ($product['options'] as $option) { ?>
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div class="item">
                                                <?php echo $option_value['name']; ?>:
                                                <?php if ($option_value['full_price']) { ?>
                                                    <span class="price">
                                                            <? if ($option_value['special_price']) { ?>
                                                                <span class="price-old"><?php echo $option_value['full_price']; ?></span> <? echo $option_value['special_price']; ?>
                                                            <?php } else { ?>
                                                                <?php echo $option_value['full_price']; ?>
                                                            <?php } ?></span>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php } ?>
                        </td>


                    </tr>
                    <?php if ($review_status) { ?>
                        <tr>
                            <td><?php echo $text_rating; ?></td>
                            <?php foreach ($products as $product) { ?>
                                <td class="rating">
                                    <div class="reviews__rating">
                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                            <?php if ($product['rating'] < $i) { ?>
                                                <span class="fa fa-stack "><i class="fa fa-star"></i></span>
                                            <?php } else { ?>
                                                <span class="fa fa-stack active"><i class="fa fa-star"></i></span>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>

                    <tr>
                        <td><?php echo $text_manufacturer; ?></td>
                        <?php foreach ($products as $product) { ?>
                            <?php if($product['manufacturer']) { ?>
                                <td><?php echo $product['manufacturer']; ?></td>
                            <?php } else { ?>
                                <td> - </td>
                            <?php }?>
                        <?php } ?>
                    </tr>

                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                        <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                            <tr>
                                <td><?php echo $attribute['name']; ?></td>
                                <?php foreach ($products as $product) { ?>
                                    <?php if (isset($product['attribute'][$key])) { ?>
                                        <td><?php echo $product['attribute'][$key]; ?></td>
                                    <?php } else { ?>
                                        <td>-</td>
                                    <?php } ?>
                                <?php } ?>
                            </tr>

                        <?php } ?>
                    <?php } ?>
                    <tr>
                        <td></td>
                        <?php foreach ($products as $product) { ?>
                            <td>
                                <input type="button" value="<?php echo $button_cart; ?>" class="btn btn-default" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" />

                            </td>
                        <?php } ?>
                    </tr>
                    </tbody>
                </table>
            <?php } else { ?>
                <p><?php echo $text_empty; ?></p>
                <div class="buttons clearfix">
                    <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_continue; ?></a></div>
                </div>
            <?php } ?>
        </div>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
