<?php echo $header; ?>
<div class="container">
    <div class="row">
    <div id="content" class="col-sm-12"><?php echo $content_top; ?>
        <div class="row">
            <?php echo $column_left; ?>
            <div class="col-md-9 col-sm-12">

                <?php if ($products) { ?>
                    <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="sort_panel">
                                <h1><?php echo $heading_title; ?></h1>
                                <?php if($sorts){ ?>
                                    <div class="sort_wrapp">
<!--                                        <label class="control-label" for="input-sort">--><?php //echo $text_sort; ?><!--</label>-->
                                        <div class="wrapp_select">
                                            <select id="input-sort" class="form-control" onchange="location = this.value;">
                                                <?php foreach ($sorts as $sorts) { ?>
                                                    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                                        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div id="mfilter_load">
                        <div class="row">
                            <?php foreach ($products as $product) { ?>
                                <?php $this->partial('product_item', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <?php if($pagination){ ?>
                            <div class="col-sm-12"><?php echo $pagination; ?></div>
                        <?php } ?>
                    </div>
                <?php }?>
                <?php if ($categories && !$products) { ?>
                    <p><?php echo $text_empty; ?></p>
                    <div class="buttons">
                        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                    </div>
                <?php } ?>
                <?php if (!$categories && !$products) { ?>
                    <h1><?php echo $heading_title; ?></h1>
                    <p><?php echo $text_empty; ?></p>
                    <div class="buttons">
                        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                    </div>
                <?php } ?>
            </div>
        </div>
      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
    </div>
</div>

 <!-- Modal -->

<?php echo $footer; ?>
