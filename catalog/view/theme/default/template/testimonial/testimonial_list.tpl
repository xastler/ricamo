<?php if ($reviews) { ?>
    <div class="row">
        <?php foreach ($reviews as $review) { ?>
            <div class="col-md-12">
                <div class="reviews__item clearfix">
                    <div class="col-md-3 col-sm-3">
                        <div class="row">
                            <div class="reviews__user">
                                <div class="flex-row container_margin">
                                    <div class="reviews__image">
                                        <i class="icon-i-person"></i>
                                    </div>

                                    <p class="reviews__username"><?php echo $review['author']; ?></p>
                                </div>

                                <div class="reviews__rating">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <?php if ($review['rating'] < $i) { ?>
                                            <span class="fa fa-stack "><i class="fa fa-star"></i></span>
                                        <?php } else { ?>
                                            <span class="fa fa-stack active"><i class="fa fa-star"></i></span>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <span><?php echo $review['date_added']; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9">
                        <div class="row">
                            <div class="description-conteiner">
                                <p class="reviews__description">
                                    <?php echo $review['text']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>


    <div class="row">
        <div class="col-sm-12"><?php echo $pagination; ?></div>
    </div>
<?php } else { ?>
    <p><?php echo $text_no_reviews; ?></p>
<?php } ?>
