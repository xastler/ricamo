<?php if (0) { ?>
    <div class="list-category">
        <?php $k = '';
        foreach ($categories as $category) {
            $k++; ?>
            <div class="list-category-title">
                <?php if ($k == 2) { ?>
                    <span class="icon-icon-tech"></span>
                <?php } else { ?>
                    <span class="icon-book"></span>
                <?php } ?>
                <?php echo $category['name']; ?><span class="icon-arrow-up active"></span>
            </div>
            <nav>
                <ul>
                    <?php foreach ($category['children'] as $child) { ?>
                        <?php if ($child['category_id'] == $child_id) {
                            $class_active = 'active';
                        } else {
                            $class_active = '';
                        } ?>
                        <li class="<?php echo $class_active ?>">
                            <a href="<?php echo $child['href']; ?>">
                                <?php if ($child['thumb']) { ?>
                                    <img src="<?php echo $child['thumb']; ?>" alt="">
                                <?php } ?>
                                <?php echo $child['name']; ?>

                            </a>
                            <?php if ($child['level_three']) { ?>
                                <span class="icon-arrow <?php echo $class_active ?>"></span>
                                <ul class="list-lvl  <?php echo $class_active ?>">
                                    <?php foreach ($child['level_three'] as $three) { ?>
                                        <?php if ($three['category_id'] == $three_id) {
                                            $class_active = 'active';
                                        } else {
                                            $class_active = '';
                                        } ?>
                                        <li class="<?php echo $class_active ?>">
                                            <a href="<?php echo $three['href']; ?>"><?php echo $three['name']; ?></a>
                                            <?php if ($three['level_three']) { ?>
                                                <span class="icon-arrow <?php echo $class_active ?>"></span>
                                                <ul class="list-lvl  <?php echo $class_active ?>">
                                                    <?php foreach ($three['level_three'] as $three_li) { ?>
                                                        <?php if ($three_li['category_id'] == $three_li_id) {
                                                            $class_active = 'active';
                                                        } else {
                                                            $class_active = '';
                                                        } ?>
                                                        <li class="<?php echo $class_active ?>">
                                                            <a href="<?php echo $three_li['href']; ?>"><?php echo $three_li['name']; ?></a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            </nav>
        <?php } ?>
    </div>
<?php } ?>


<div class="list-category">
    <div class="list-category-title">
        <span class="icon-book"></span>
        Каталог<span class="icon-arrow-up active"></span>
    </div>
    <nav>
        <ul>
            <?php foreach ($categories as $category) { ?>
                <?php if ($category['category_id'] == $category_id) {
                    $class_active = 'active';
                } else {
                    $class_active = '';
                } ?>
                <li class="<?php echo $class_active ?>">
                    <a href="<?php echo $category['href']; ?>">
                        <?php if ($category['thumb']) { ?>
                            <img src="<?php echo $category['thumb']; ?>" alt="">
                        <?php } ?>
                        <?php echo $category['name']; ?>

                    </a>
                    <?php if ($category['children']) { ?>
                        <span class="icon-arrow <?php echo $class_active ?>"></span>
                        <ul class="list-lvl  <?php echo $class_active ?>">
                            <?php foreach ($category['children'] as $child) { ?>
                                <?php if ($child['category_id'] == $child_id) {
                                    $class_active = 'active';
                                } else {
                                    $class_active = '';
                                } ?>
                                <li class="<?php echo $class_active ?>">
                                    <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                    <?php if ($child['level_three']) { ?>
                                        <span class="icon-arrow <?php echo $class_active ?>"></span>
                                        <ul class="list-lvl  <?php echo $class_active ?>">
                                            <?php foreach ($child['level_three'] as $three) { ?>
                                                <?php if ($three['category_id'] == $three_id) {
                                                    $class_active = 'active';
                                                } else {
                                                    $class_active = '';
                                                } ?>
                                                <li class="<?php echo $class_active ?>">
                                                    <a href="<?php echo $three['href']; ?>"><?php echo $three['name']; ?></a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </nav>
    <div class="list-category-title">
        <span class="icon-icon-tech"></span>
        Виробники<span class="icon-arrow-up active"></span>
    </div>
    <nav>
        <ul>
            <?php foreach ($manufacturers as $manufacturer) { ?>
                <li>
                    <a href="<?php echo $manufacturer['href']; ?>">
                        <?php echo $manufacturer['name']; ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </nav>
  </div>

