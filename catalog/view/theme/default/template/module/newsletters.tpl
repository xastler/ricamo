<script>
		function subscribe()
		{
			var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			var email = $('#txtemail').val();
			if(email != "")
			{
				if(!emailpattern.test(email))
				{
				    $('.alert, .text-danger').remove();
                    $('#xx-tovars .modal-top').prepend('<div class="alert"><i class="fa fa-exclamation-circle"></i>Неправильно введений E-mail</div>');
					return false;
				}
				else
				{
					$.ajax({
						url: 'index.php?route=module/newsletters/news',
						type: 'post',
						data: 'email=' + $('#txtemail').val(),
						dataType: 'json',

						success: function(json) {
                            $('.alert, .text-danger').remove();
                            $('#xx-tovars .modal-top').prepend('<div class="alert"><i class="fa fa-check-circle"></i>Дякуємо за підписку на новини</div>');
                        }
						
					});
					return false;
				}
			}
			else
			{
			    $('.alert, .text-danger').remove();
                $('#xx-tovars .modal-top').prepend('<div class="alert"><i class="fa fa-exclamation-circle"></i>Заповніть поле</div>');
				$(email).focus();
				return false;
			}
			

		}
	</script>
    <h5><?php echo $heading_title; ?></h5>
    <form action="" class="footer-subscription" method="post">
        <input type="email" name="txtemail" id="txtemail" value="" placeholder="Email" required>
        <button type="submit" onclick="return subscribe();" data-toggle="modal" data-target=".bs-example-modal-sm" >
            <svg xmlns="http://www.w3.org/2000/svg" width="37" height="37">
                <path class="sircle" fill="#F65314" fill-rule="evenodd" d="M17.999-.001c9.941 0 18 8.059 18 18.001 0 9.941-8.059 17.999-18 17.999s-18-8.058-18-17.999c0-9.942 8.059-18.001 18-18.001z"/>
                <path fill="none" stroke="#FFF" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.998 18h16M19.999 24.999l6-6.999-6-7"/>
            </svg>
        </button>
    </form>
