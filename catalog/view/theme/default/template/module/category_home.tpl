<div class="img-list-category">
    <h3><?php echo $heading_title; ?></h3>
    <?php $k=0; foreach ($categories as $category) { $k++; ?>
        <?php if($k==1){ $category_link= $category['href']; } ?>
        <?php foreach ($category['children'] as $child) { ?>
            <div class="title"><?php echo $child['name']; ?></div>
            <div class="row">
                <?php foreach ($child['level_three'] as $child_three) { ?>
                    <div class="col-sm-4 col-xs-6">
                        <a href="<?php echo $child_three['href']; ?>">
                            <div class="block-img">
                                <img src="<?php echo $child_three['thumb']; ?>" alt="<?php echo $child_three['name']; ?>">
                            </div>
                            <p><?php echo $child_three['name']; ?></p>
                        </a>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    <?php } ?>
</div>
<a class="btn-olt" href="<?php echo $category_link; ?>"><span><?php echo $text_btn; ?></span></a>
<span class="lines"></span>