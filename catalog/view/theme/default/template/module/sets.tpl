<?php 

if($sets)
{
?>
<script src="catalog/view/javascript/sets/script.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/slick/slick-theme.css"/>
<script type="text/javascript" src="catalog/view/javascript/slick/slick.min.js"></script>
<link href="catalog/view/javascript/sets/style.css" rel="stylesheet" type="text/css" />

<script>
    $(document).ready(function () {
       $('.sets').insert<?php echo $position;?>('<?php echo $selector;?>');
        
        $('.slick').slick({infinite: true,speed: 300,slidesToShow: 1,adaptiveHeight: true,arrows: true});
    });
    
</script>
<div class='sets'>
    <h3><?php echo $text_sets;?></h3>
    <div class='slick'>
    <?php 

    foreach($sets as $set) 
    {
    ?>
    <div class='set'>
        <div class='set_table'>
        <?php
        $i=0;
        $c=count($set['products']); 
        $num=$c+$c-1+2;
        $aonum=($num-1)*10;
        foreach($set['products'] as $product) { 

        ?>
        <?php if($i++) { ?>
        <div class='cell ao'>+</div>
        <?php } ?>
        <div class='cell set-product' >
            <?php if($show_disc_prec) { ?>
                <?php if($product['discount_prec']) { ?>
                    <div class='disc'>-<?php echo $product['discount_prec'];?>%</div>
                <?php } ?>
            <?php } ?>
             <?php if($product['thumb']) { ?>
             <a href="<?php echo $product['href']; ?>"><img src='<?php echo $product['thumb']; ?>' class='img-responsive'></a>
            <?php } ?>
            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['product_name']; ?></a></h4>
            <?php if($show_old_price) { ?>
                <?php if($product['old_price']!=$product['new_price']) { ?>
                    <span class='old_price'><?php echo $product['old_price']; ?></span>
                <?php } ?>
            <?php } ?>
            <span class='new_price'><?php echo $product['new_price']; ?></span>
            <?php if($show_qty) { ?>
                <?php if($product['quantity']>1) { ?>
                    <div class='quantity'>x<?php echo $product['quantity']; ?></div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php } 
        ?>
        <div class='cell ao'>=</div>
        <div class='cell total'>
            <?php if($show_disc_prec) { ?>
                <?php if($set['set_discount_prec']) { ?>
                    <div class='disc'>-<?php echo $set['set_discount_prec'];?>%</div>
                <?php } ?>
            <?php } ?>
            <div>
                <?php if($show_old_price) { ?>
                <?php if($set['old_summ']!=$set['new_summ']) { ?> <div class='old_summ'><?php echo $set['old_summ']; ?></div> <?php } ?>
                <?php } ?>
                <div class='new_summ'><?php echo $set['new_summ']; ?></div>
            </div>
            <button class='add-set-btn btn btn-danger' onclick='addSet(<?php echo $set["js_array"];?>,"<?php echo $set["set_discount_fixed"];?>");'><?php echo $buy_sets;?></button>
        </div>
        </div>
    </div>
    <?php } ?>
    </div>
</div>

<?php } ?>