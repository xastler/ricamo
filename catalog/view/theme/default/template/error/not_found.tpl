<?php echo $header; ?>

    <div class="error-404">
        <div class="container">
            <div class="error-404__logo">
                <a href="<?php echo $continue; ?>">
                    <img src="<?php echo $logo; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                </a>
            </div>
            <div class="error-404__img-bg">

            </div>
            <h1><?php echo $heading_title; ?></h1>
            <p><?php echo $text_error; ?></p>
            <div class="error-404__btn">
                <a href="<?php echo $continue; ?>">
                    <span><?php echo $button_error; ?></span>
                </a>
            </div>
        </div>
    </div>
<?php echo $footer; ?>