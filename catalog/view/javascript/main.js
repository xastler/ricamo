$(document).ready(function () {
    $().fancybox({
        selector : '[data-fancybox="gallery"]',
        loop     : true
    });
    if ($(window).width() <= '767') {
        $('.header-navigation_media').append($('.top-bar-navigation'));
        $('.inf-tabs-1 li:nth-child(1)').append($('#delivery-1'));
        $('.inf-tabs-1 li:nth-child(2)').append($('#delivery-2'));
        $('.inf-tabs-2 li:nth-child(1)').append($('#payment-1'));
        $('.inf-tabs-2 li:nth-child(2)').append($('#payment-2'));
        $('.inf-tabs-2 li:nth-child(3)').append($('#payment-3'));
    }
    $(window).resize(function () {
        if ($(window).width() <= '767') {
            $('.header-navigation_media').append($('.top-bar-navigation'));
            return this;
        }
        else {
            $('#top .top-bar').append($('.top-bar-navigation'));
        }
    });

    $('.list-category nav span.icon-arrow').click(function () {
        $(this).toggleClass("active");
        $(this).next('ul').slideToggle();
    });

    $(function() {
        $('#input-sort').selectric();
        $('._product_content').append($('.sets'));
        $(".product-page .main_thumb img").imagezoomsl({
            innerzoommagnifier: true,
            classmagnifier: window.external ? window.navigator.vendor === "Yandex" ? "" : "round-loupe" : "",
            magnifierborder: "5px solid #F0F0F0",
            zoomrange: [2, 2],
            zoomstart: 2,
            magnifiersize: [150, 150]
        });
    });
    $("input[type=tel], input[name=\"phone\"], input[name=\"telephone\"]").mask("+38(099) 999-99-99");
    $('.header-menu-btn').click(function () {
        $('body').addClass('active');
        $('.page-content').addClass('active');
        $('.panel-menu').addClass('active');
    });
    $('.panel-menu-close').click(function () {
        $('body').removeClass('active');
        $('.page-content').removeClass('active');
        $('.panel-menu').removeClass('active');
    });

    var h_hght = 74; // высота шапки
    var h_mrg = 0;    // отступ когда шапка уже не видна
    $(function(){
        var elem = $('header');
        var top = $(this).scrollTop();
        if(top > h_hght){
            elem.css('top', h_mrg);
        }
        $(window).scroll(function(){
            top = $(this).scrollTop();

            if (top+h_mrg < h_hght) {
                elem.css('top', (h_hght-top));
            } else {
                elem.css('top', h_mrg);
            }
        });
    });

    $('.header-phone .icon-arrow').click(function () {
        $('.header-phone').toggleClass('active');
        $(this).toggleClass('active');
        $('.header-phone-nav').slideToggle();
    });

    $('.header-phone .icon-phone').click(function () {
        $('.header-phone').toggleClass('active');
        $('.header-phone-nav').slideToggle();
    });
    $('.list-category-title .icon-arrow-up').click(function () {
        $(this).toggleClass('active');
        $(this).parent().next('nav').find('ul').slideToggle();
    });

    $('.filter_heading .icon-arrow-up').click(function () {
        $(this).toggleClass('active');
        $(this).closest('.mfilter-box').find('.mfilter-content').slideToggle();
    });



    //SLIDER
    $('.slidershow').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    });

    $("input[name=quantity], #simplecheckout_cart .quantity input").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $(document).on('click', '.qty_wrapp .bnt_plus', function () {
        var num = $('.qty_wrapp input[name=quantity]').val();
        var min = parseInt($('.qty_wrapp input[name=quantity]').data('min'));
        $('.qty_wrapp input[name=quantity]').val(parseInt(num)+parseInt($('.qty_wrapp input[name=quantity]').data('min')));
    });
    $(document).on('click', '.qty_wrapp .bnt_minus', function () {
        if($('.qty_wrapp input[name=quantity]').val() > $('.qty_wrapp input[name=quantity]').data('min')) {
            var num = $('.qty_wrapp input[name=quantity]').val();
            $('.qty_wrapp input[name=quantity]').val(parseInt(num) - parseInt($('.qty_wrapp input[name=quantity]').data('min')));
        }
    });


    $(document).on('click', '.share', function () {
        $(this).fadeOut(100, function () {
            $('.share_buttons').addClass('active');
        });
    });

});


