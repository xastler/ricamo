function addSet(pinfo, discount)
{
    $.ajax({
        url: 'index.php?route=module/sets/addSet',
        type: 'POST',
        data: {pinfo: pinfo, discount: discount},
        success: function (json) {


            if (json['redirect'])
                location = json['redirect'];
        }
    });
}